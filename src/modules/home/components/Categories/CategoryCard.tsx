import { Box, Typography } from "@mui/material";
import React from "react";

const CategoryCard = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        backgroundColor: "rgba(30, 91, 99, 0.2)",
        border: "3px solid #f8f8f8f8",
        borderRadius: "20px",
        width: "150px",
        height: "150px",
        padding: "15px",
        boxSizing: "border-box",
      }}
    >
      <Typography
        sx={{ color: "primary.main", fontSize: "19px", fontWeight: "700" }}
      >
        AZ
      </Typography>
      <Typography
        sx={{
          color: "primary.main",
          fontStyle: "italic",
          fontSize: "18px",
          padding: "0 0 10px 0",
        }}
      >
        English Language
      </Typography>
    </Box>
  );
};

export default CategoryCard;
