import * as React from "react";
import Button from "@mui/material/Button";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import CategoriesIcon from "assets/icons/CategoriesIcon";

export default function CustomizedMenus() {
  return (
    <div>
      <Button
        id="demo-customized-button"
        aria-haspopup="true"
        variant="default"
        disableElevation
        endIcon={<KeyboardArrowDownIcon />}
        startIcon={<CategoriesIcon />}
      >
        Options
      </Button>
    </div>
  );
}
