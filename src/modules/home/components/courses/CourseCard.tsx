import { Avatar, Box, Button, Rating, Typography } from "@mui/material";
import React, { FC } from "react";
import productImg from "assets/images/product.png";
import TimeIcon from "assets/icons/time.svg";
import Video from "assets/icons/video.svg";
import Level from "assets/icons/level.svg";
import Male from "assets/icons/male.svg";

const CourseCard = () => {
  const [value, setValue] = React.useState<number | null>(2);

  return (
    <Box
      sx={{
        borderRadius: "5px",
        boxShadow: "1px 2px 5px rgba(0, 0, 0, 0.25)",
        width: "300px",
        minWidth: "250px",
      }}
    >
      <Box
        sx={{
          minWidth: "250px",
          height: "200px",
          backgroundImage: `url(${productImg})`,
          backgroundPosition: "left",
          backgroundSize: "cover",
        }}
      ></Box>
      <Box sx={{ px: 2, pt: 1.5 }}>
        <Typography
          sx={{ color: "secondary.main", fontSize: "19px", fontWeight: 700 }}
        >
          English language course
        </Typography>
        <Box sx={{ mt: "9px" }}>
          <Rating
            name="simple-controlled"
            value={value}
            size="small"
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            columnGap: "10px",
            borderBottom: "0.5px solid rgba(30, 91, 99, 0.19)",
            py: 1.5,
          }}
        >
          <Avatar
            sx={{ width: "35px", height: "35px", border: "3px solid #FFD700" }}
          />
          <Typography sx={{}}>Housam</Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            columnGap: "10px",
            borderBottom: "0.5px solid rgba(30, 91, 99, 0.19)",
            py: 1.5,
            flexWrap: "wrap",
            justifyContent: "center",
            rowGap: "10px",
          }}
        >
          <SmallInfo title="6 hours" icon={TimeIcon} />
          <SmallInfo title="12 Lessons" icon={Video} />
          <SmallInfo title="Beginner" icon={Level} />
          <SmallInfo title="Male" icon={Male} />
        </Box>
        <Box
          sx={{
            py: 1.5,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Button>Reservation</Button>
          <Typography
            sx={{ fontSize: "18px", fontWeight: "700", color: "primary.main" }}
          >
            5000 SAR
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default CourseCard;

interface SmallInfoProps {
  title: string;
  icon: any;
}

const SmallInfo: FC<SmallInfoProps> = (props) => {
  return (
    <Box sx={{ display: "flex", alignItems: "center", columnGap: "5px" }}>
      <img src={props.icon} alt="" />
      <Typography sx={{ fontSize: "11px" }}>{props.title}</Typography>
    </Box>
  );
};
