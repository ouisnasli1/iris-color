import { Box, Typography } from "@mui/material";
import React, { FC } from "react";
import CategoryCard from "../components/Categories/CategoryCard";
import CourseCard from "../components/courses/CourseCard";
import CourseSlider from "../components/courses/CourseSlider";

const HomeIndex = () => {
  return (
    <Box sx={{ marginTop: "50px", pb: 15 }}>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Box sx={{ width: "80%" }}>
          <Typography
            sx={{ color: "primary.main", fontSize: "35px", fontWeight: "700" }}
          >
            Categories
          </Typography>
          <Box
            sx={{
              mt: 5,
              display: "flex",
              flexWrap: "wrap",
              columnGap: "18px",
              rowGap: "18px",
              justifyContent: "center",
            }}
          >
            {[1, 2, 3, 4, 5, 6, 7, 8].map((item, index) => (
              <CategoryCard key={index} />
            ))}
          </Box>
        </Box>
      </Box>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Box sx={{ width: "80%" }}>
          <Typography
            sx={{ color: "primary.main", fontSize: "35px", fontWeight: "700" }}
          >
            Courses
          </Typography>
          <Box
            sx={{
              mt: 5,
              display: "flex",
              flexWrap: "wrap",
              columnGap: "18px",
              rowGap: "18px",
              justifyContent: "center",
            }}
          >
            <CourseSlider>
              {[1, 2, 3, 4, 5].map((item, index) => (
                <CourseCard key={index} />
              ))}
            </CourseSlider>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default HomeIndex;
