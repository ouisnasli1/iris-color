import { Box } from "@mui/material";
import React from "react";
import { Outlet } from "react-router-dom";
import Header from "../../components/layout/Header";

const HomeComponent = () => {
  return (
    <>
      <Header />
      <Box
        sx={{
          background:
            "background: linear-gradient(252.31deg, #edffee -0.77%, #fff0e1 25.26%, #fff6e9 50.14%,  #edffee 75.86%, #ffffff 100% )",
        }}
      >
        <Outlet />
      </Box>
    </>
  );
};

export default HomeComponent;
