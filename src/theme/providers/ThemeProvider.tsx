import { CssBaseline } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import React, { FC } from "react";
import { createTheme } from "theme";

interface ThemeProviderWraperProps {
  children: any;
}

export const ThemeProviderWraper: FC<ThemeProviderWraperProps> = ({
  children,
}) => {
  const theme = createTheme({ responsiveFontSizes: true });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};
