import { ThemeOptions } from "@mui/material";

const neutral = {
  100: "#F3F4F6",
  200: "#E5E7EB",
  300: "#D1D5DB",
  400: "#9CA3AF",
  500: "#6B7280",
  600: "#4B5563",
  700: "#374151",
  800: "#1F2937",
  900: "#111827",
};

const divider = "#E6E8F0";

const text = {
  primary: "#121828",
  secondary: "#65748B",
  disabled: "rgba(55, 65, 81, 0.48)",
};

declare module "@mui/material/Button" {
  interface ButtonPropsVariantOverrides {
    error: true;
    success: true;
    primary: true;
    default: true;
  }

  interface ButtonPropsColorOverrides {
    error: true;
    success: true;
    primary: true;
    default: true;
  }
}

export const lightThemeOptions: ThemeOptions = {
  components: {
    MuiButton: {
      variants: [
        {
          props: { variant: "primary" },
          style: {
            color: "#1E5B63",
            border: "3px",
            borderColor: "#EEEEEE",
            borderStyle: "solid",
            borderRadius: "10px",
            backgroundColor: "inherit",
            minWidth: "10px",
            padding: "5px",
          },
        },
        {
          props: { variant: "default" },
          style: {
            color: "#1E5B63",

            backgroundColor: "inherit",
          },
        },
      ],
    },
    MuiAvatar: {
      styleOverrides: {
        root: {
          backgroundColor: neutral[500],
          color: "#FFFFFF",
        },
      },
    },

    MuiMenu: {
      styleOverrides: {
        paper: {
          borderColor: divider,
          borderStyle: "solid",
          borderWidth: 1,
        },
      },
    },
  },
  palette: {
    mode: "light",
    background: { default: "#ffffff", paper: "#E3F2FD" },
    primary: {
      main: "#1E5B63",
    },
    secondary: {
      main: "#555555",
    },
    success: {
      light: "#B9F6CA",
      main: "#69F0AE",
      dark: "#00C853",
      200: "#69F0AE",
    },
    error: {
      light: "#EF9A9A",
      main: "#F44336",
      dark: "#C62828",
    },
    warning: {
      light: "#B9F6CA",
      main: "#FFE57F",
      dark: "#FFC107",
    },
  },
  typography: {
    // fontFamily: ["ManropeBold", "ManropeRegular"].join(","),

    button: {
      fontSize: "14px",
    },

    h1: {
      fontSize: "64px",
      fontWeight: "bold",
    },
    h2: {
      fontSize: "48px",
      fontWeight: "bold",
    },
    h3: {
      fontSize: "40px",
      fontWeight: "bold",
    },
    h4: {
      fontSize: 26,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 23,
      fontWeight: "bold",
    },
    h6: {
      fontSize: 20,
    },

    subtitle1: {
      fontSize: "0.955rem",
    },
    subtitle2: {
      fontSize: 12,
    },

    body1: {
      fontSize: "0.875rem",
      fontWeight: "500",
    },
    body2: {
      fontSize: "14px",
    },
    caption: {
      fontSize: "12px",
    },
  },

  shadows: [
    "none",
    "0px 1px 1px rgba(100, 116, 139, 0.06), 0px 1px 2px rgba(100, 116, 139, 0.1)",
    "0px 1px 2px rgba(100, 116, 139, 0.12)",
    "0px 1px 4px rgba(100, 116, 139, 0.12)",
    "0px 1px 5px rgba(100, 116, 139, 0.12)",
    "0px 1px 6px rgba(100, 116, 139, 0.12)",
    "0px 2px 6px rgba(100, 116, 139, 0.12)",
    "0px 3px 6px rgba(100, 116, 139, 0.12)",
    "0px 2px 4px rgba(31, 41, 55, 0.06), 0px 4px 6px rgba(100, 116, 139, 0.12)",
    "0px 5px 12px rgba(100, 116, 139, 0.12)",
    "0px 5px 14px rgba(100, 116, 139, 0.12)",
    "0px 5px 15px rgba(100, 116, 139, 0.12)",
    "0px 6px 15px rgba(100, 116, 139, 0.12)",
    "0px 7px 15px rgba(100, 116, 139, 0.12)",
    "0px 8px 15px rgba(100, 116, 139, 0.12)",
    "0px 9px 15px rgba(100, 116, 139, 0.12)",
    "0px 10px 15px rgba(100, 116, 139, 0.12)",
    "0px 12px 22px -8px rgba(100, 116, 139, 0.25)",
    "0px 13px 22px -8px rgba(100, 116, 139, 0.25)",
    "0px 14px 24px -8px rgba(100, 116, 139, 0.25)",
    "0px 10px 10px rgba(31, 41, 55, 0.04), 0px 20px 25px rgba(31, 41, 55, 0.1)",
    "0px 25px 50px rgba(100, 116, 139, 0.25)",
    "0px 25px 50px rgba(100, 116, 139, 0.25)",
    "0px 25px 50px rgba(100, 116, 139, 0.25)",
    "0px 25px 50px rgba(100, 116, 139, 0.25)",
  ],
};
