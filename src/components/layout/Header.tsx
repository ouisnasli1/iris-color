import { Avatar, Badge, Box, Button, IconButton } from "@mui/material";
import SearchInput from "components/SearchInput";
import CustomizedMenus from "modules/home/components/Menu";
import React from "react";
import Logo from "../../assets/images/Logo.svg";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import Placement from "assets/icons/Placement";
import notifcation from "assets/icons/notifcation.svg";
import flagIcon from "assets/icons/flag.png";

const Header = () => {
  return (
    <Box
      sx={{
        height: "75px",
        display: "flex",
        alignItems: "center",
        px: 15,
        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.15)",
        position: "sticky",
        top: "0",
        backgroundColor: "#fff",
        zIndex: "10000",
      }}
    >
      <Box sx={{ width: "97px", mr: 3 }}>
        <img src={Logo} alt="" width="100%" />
      </Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Box sx={{ display: "flex", flex: "0.38" }}>
          <CustomizedMenus />
          <Button variant="default">Courses</Button>
          <Button variant="default">Blogs</Button>
          <Button variant="default">Contact Us</Button>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            columnGap: "20px",
            flex: "0.4",
          }}
        >
          <SearchInput />
          <Button variant="primary" sx={{ minWidth: "10px" }}>
            <MoreHorizIcon />
          </Button>
          <Button startIcon={<Placement />} variant="default">
            Placement Test
          </Button>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flex: "0.17",
          }}
        >
          <IconButton>
            <img src={flagIcon} />
          </IconButton>
          <IconButton sx={{ backgroundColor: "inherit" }}>
            <Badge
              color="error"
              badgeContent={3}
              showZero
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
            >
              <img src={notifcation} alt="" />
            </Badge>
          </IconButton>
          <Box>
            <Avatar>H</Avatar>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Header;
