import React from "react";
import Input from "@mui/material/Input";
import { FormControl, InputAdornment } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

const SearchInput = () => {
  return (
    <FormControl>
      <Input
        id="input-with-icon-adornment"
        placeholder="Search"
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </FormControl>
  );
};

export default SearchInput;
