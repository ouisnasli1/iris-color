import React from "react";
import { Route, Routes } from "react-router-dom";
import HomeRouting from "./modules/home/HomeRouting";

function App() {
  return (
    <Routes>
      <Route path="*" element={<HomeRouting />} />
    </Routes>
  );
}

export default App;
